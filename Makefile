SHELL := /bin/bash
PROJECT_ROOT := $(dir $(firstword $(MAKEFILE_LIST)))

.PHONY: noop
noop:
	@echo "No default action specified."

.PHONY: test
test:
	@python -m unittest discover

.PHONY: tox
tox:
	@tox -e py37,py38,py39,flake8 --skip-missing-interpreters

.PHONY: build
build:
	@flit build

.PHONY: publish
publish:
	@dotenv run -- twine upload dist/*

.PHONY: clean
clean:
	@echo "Cleaning up tox data.."
	rm -rf .tox
	@echo "Cleaning up build data.."
	rm -rf dist
