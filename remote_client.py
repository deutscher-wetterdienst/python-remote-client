#!/usr/bin/env python3
"""Combined SSH and SCP client for Python."""

import logging
import os
from contextlib import AbstractContextManager
from os import PathLike
from typing import Optional, Union

from paramiko import SSHClient
from scp import SCPClient

__version__ = "0.2.2"


class RemoteClient(AbstractContextManager):
    """Client to interact with a remote host via SSH & SCP."""

    def __init__(self, hostname: str, *, client: Optional[SSHClient] = None, **kwargs):
        self.logger = logging.getLogger(self.__class__.__name__)
        if client is None:
            client = SSHClient()
            client.load_system_host_keys()
        self.client = client
        self.client.connect(hostname, **kwargs)
        self.logger.info(f"Successfully connected to {hostname}.")
        self.scp = SCPClient(self.client.get_transport())
        self.logger.info(f"Successfully connected to {hostname}.")

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def close(self):
        """Close SSH & SCP connection."""
        self.scp.close()
        self.client.close()
        self.logger.info(f"Connection closed.")

    def get(self, *remote_paths: Union[str, PathLike], local_path: Union[str, PathLike] = "",
            recursive=False, preserve_times=False):
        """Download files from remote host."""
        for remote_path in map(os.fspath, remote_paths):
            self.scp.get(remote_path, local_path, recursive, preserve_times)
        self.logger.info(f"Finished downloading {len(remote_paths)} files to {local_path}.")

    def put(self, *local_paths: Union[str, PathLike], remote_path: Union[str, PathLike] = ".",
            recursive=False, preserve_times=False):
        """Upload files to remote host."""
        local_paths = list(map(os.fspath, local_paths))
        self.scp.put(local_paths, remote_path, recursive, preserve_times)
        if remote_path == ".":
            remote_path = "remote CWD"
            # todo: resolve remote CWD
        self.logger.info(f"Finished uploading {len(local_paths)} files to {remote_path}.")

    def execute_command(self, *commands: str,
                        bufsize=-1, timeout=None, get_pty=False, environment=None, ignore_errors=False):
        """Execute commands on remote host."""
        outputs = []
        for command in commands:
            self.logger.info(f"Executing command: {command}")
            stdin, stdout, stderr = self.client.exec_command(command, bufsize, timeout, get_pty, environment)
            exit_code = stdout.channel.recv_exit_status()
            response = stdout.read()
            self.logger.info(f"Finished with exit code {exit_code}.")
            if response:
                self.logger.info("Command Output:")
                self.logger.info(response)
            else:
                self.logger.info("No command output received.")
            if exit_code != 0 and not ignore_errors:
                raise RuntimeWarning(f'Command "{command}" returned non-zero exit code: {exit_code}')
            outputs.append(response)
        return outputs
