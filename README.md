# Python Remote Client

Combined SSH and SCP client for Python.

## Usage

The CI deploys the _remote_client_ python package to GitLab's
package registry. To install it with pip, follow these steps:

0. Create a personal access token in your GitLab account's settings.
0. If you want to have your credentials to GitLab's pypi stored in your system's
   keyring, you need to have the python package _keyring_ installed. It is a
   dependency of _twine_ so you probably have it already.
0. `pip install remote-client --extra-index-url https://gitlab.com/api/v4/projects/24020287/packages/pypi/simple`
0. When asked for credentials, use `__token__` as username and enter your
   personal access token as the password. You will also be asked whether or not to
   store your credentials with _keyring_.

## Develop
### Setting up dev environment

0. Have Python 3.6+, prefferably in a virtual env, i.e. `python3 -m virtualenv venv`
0. Install dev requirements via `pip install -r requirements.txt`

### Making a new release

Create releases from tagged commits. To bump the semantic version, use
[bump2version](https://github.com/c4urself/bump2version):

```
bumpversion major|minor|patch
```

This will bump the version (specified in the file `.bumpversion.cfg`) and create a
new commit tagged with the bumped version. Push the commit and the tag to the remote
and the GitLab-CI will do the rest.

> Do not skip _bump2version_ by creating the git tags manually as by doing so,
> the git tag and _bump2version_ will eventually become out of sync.

## Publish manually to GitLab's package registry

0. Create a GitLab personal access token with API access.
0. Configure environment variables: `cp example.env .env`
0. Publish via `make build && make publish`.
0. On first run, enter API token when prompted for password. This will then be
   saved to your keyring.

### Testing CI

When making changes to `.gitlab-ci.yml` it is a good idea to
[test the jobs locally](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/312) before
making a commit.

## Package dependencies
- [Paramiko](https://github.com/paramiko/paramiko): SSH client
- [scp.py](https://github.com/jbardin/scp.py): SCP client

## Development tools
- [bump2version](https://github.com/c4urself/bump2version): Bump package version
- [Flake8](https://gitlab.com/pycqa/flake8): PEP8 checking
- [Flit](https://github.com/takluyver/flit): Building (and in the future publishing) the package
- [keyring](https://github.com/jaraco/keyring): Talk to OS keychain
- [python-dotenv](https://github.com/theskumar/python-dotenv): Python .env-file support
- [tox](https://github.com/tox-dev/tox): Run Python tests more easily
- [Twine](https://github.com/pypa/twine): Fallback to deploy to GitLab's package registry as flit is currently unsupported
